// src/mylib.js
module.exports = {
    sum: (a, b) => a + b,
    random: () => {
        return Math.random();
    },
    arrayGen: () => [1,2,3],
    date: () => { return Date() },
    args: () => {
        var list = [];        
        process.argv.forEach((val, idx) => {
             list[idx] = {args : idx, value : val};
        });
        if(list.length > 0)       
            return list;
    },
    
}