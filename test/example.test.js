// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const assert  = require('chai').assert;

const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {
    
    let myvar = undefined;
    let mydate = undefined;
    let myArray = undefined;
    let myArraylen = undefined;

    before(() => {        
        myvar = 1;
        mydate = Date();
        myArray = ["a", "b"]
        myArraylen = myArray.length;

        console.log('Before testing...');
        console.log(`Variables initialized before testing:  myvar = ${myvar},  date = ${mydate}, 
        arraylen = ${myArraylen}`);
    });

    it('Should return 2 when sum function a=1, b=1', () => {
        const result = mylib.sum(1,1); // 1 + 1
        expect(result).to.equal(2); // result expected to be equal to 2 
    });
    it('Parameterized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar);
        expect(result).to.equal(myvar + myvar);
    })
    it('Assert date should be current date', () => {
        const result = mylib.date();
        assert(result == mydate);
    });
    it('args array should exist', () => {
        const result = mylib.args();
        should.exist(result);
    });
    it('Expects array with lenght 2 ', () =>{
        const result = mylib.args();        
        expect(result.length).to.equal(myArraylen);
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') // true
    });

    it('myvar should exist', () => {
        should.exist(myvar);
    });

    after(() => {
        console.log('After testing...');
    });
});