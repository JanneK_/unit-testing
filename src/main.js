// src/main.js

const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');

app.listen(port, () => {
    console.log(`server: http//localhost:${port}`);
});

console.log({
    args: process.argv,
    sum: mylib.sum(5,3),
    random: mylib.random(),
    arrayGen: mylib.arrayGen()
});
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(`${mylib.sum(a,b)}`);
});
app.get('/now', (req, res) => {
    const now = mylib.date();
    res.send(`Current date is: ${now}`);
})
app.get('/args', (req, res) =>{
    let args = mylib.args();    
    if (args)
    {
        var myJson = JSON.stringify(args);
        res.status(200);
        res.send(">>> COMMAND LINE ARGUMENTS: " + myJson);    
    }       
    else
        res.sendStatus(404);
});
app.get('/', (req, res) => {
    res.send('Hello World')
});

